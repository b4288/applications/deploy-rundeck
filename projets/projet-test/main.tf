terraform {
  required_providers {
    rundeck = {
      source  = "rundeck/rundeck"
      version = "0.4.2"
    }
  }
}

provider "rundeck" {
  url         = "https://rundeck.robert.local/"
  api_version = "38"
  auth_token  = "xxxxxxxx"
}

resource "rundeck_private_key" "terraform" {
  path         = "terraform/id_rsa"
  key_material = file("~/.ssh/id_rsa")
}

resource "rundeck_project" "terraform" {
  name        = "terraform"
  description = "Sample Project Created with Terraform"
  ssh_key_storage_path = "${rundeck_private_key.terraform.path}"
  resource_model_source {
    type = "local"
    config = {
      description = "Rundeck server node"

    }
  }
  extra_config = {
    "project.label" = "Terraform Project"
  }
}

resource "rundeck_job" "testjob" {
  name              = "Job Test"
  project_name      = "${rundeck_project.terraform.name}"
  description       = "A simple commande"

  command {
    shell_command = "mount"
  }
}